void main(List<String> args) {
  //print('__EXERSICE 1______');
  List total = [1, 7, 12, 3, 56, 2, 87, 34, 54];
  print(
      'Первый элемент: ${total.first} Пятый элемент: ${total[5]} Последний эдемент: ${total.last}');

  //print('__EXERSICE 2______');
  List a = [3, 12, 43, 1, 25, 6, 5, 7];
  List b = [55, 11, 23, 56, 78, 1, 9];
  a.addAll(b);
  print(a);

  //print('__EXERSICE 3______');
  List word = [
    'a',
    'd',
    'F',
    'l',
    'u',
    't',
    't',
    'e',
    'R',
    'y',
    '3',
    'b',
    'h',
    'j'
  ];
  print(word.sublist(2, 9));

  //print('__EXERSICE 4______');
  List numFinde = [1, 2, 3, 4, 5, 6, 7];
  print(numFinde.contains(3));
  print(
      'Первый элемент:${numFinde.first} Последний элемент:${numFinde.last} Длина списка:${numFinde.length}');

  //print('__EXERSICE 5______');
  List _bool = [601, 123, 2, "dart", 45, 95, "dart24", 1];

  print('1-${_bool.contains('dart')}');
  print('2-${_bool.contains(951)}');

  //print('__EXERSICE 6______');
  List wordFinde = ["post", 1, 0, "flutter"];
  String myDart = 'Flutter';
  print(wordFinde.contains(myDart.toLowerCase()));

  //print('__EXERSICE 7______');
  List wordSort = ["I", "Started", "Learn", "Flutter", "Since", "April"];
  String myFlutter = wordSort.join(' * ');
  print(myFlutter);

  //print('__EXERSICE 8______');
  List totalSort = [1, 9, 3, 195, 202, 2, 5, 7, 9, 10, 3, 15, 0, 11];
  totalSort.sort(
    (a, b) => a.compareTo(b),
  );
  print(totalSort);
}
